﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using WpfSetup.Common;
using WpfSetup.Properties;
using Settings = CommonLib.Settings;

namespace WpfSetup
{
    /// <summary>
    /// Действия по окончанию установки.
    /// </summary>
    public static class AfterInstallAction
    {
        public static event EventHandler Finished;
        public static event EventHandler<string> ChangedStatus;
        private static VisionSetup Setup => App.Setup;

        /// <summary>
        /// Запуск всех необходимых действий по окначнию установки
        /// </summary>
        public static void RunAfterInstallAction()
        {
            if (Setup.IsUninstall)
            {
                UninstallAction();
            }
            else
            {
                InstallAction();
            }
        }

        /// <summary>
        /// Действия после удаления.
        /// </summary>
        private static void UninstallAction()
        {
            if (Setup.IsDeleteIni)
            {
                UninstallIni();
            }
        }

        /// <summary>
        /// Действия после установки.
        /// </summary>
        private static void InstallAction()
        {
            if(Setup.CancelRequested != true)
            {
                OnChangedStatus("Fix ini file ...");
                InstallIniFile();
                ChangeIniFile();

                if (!DriversHelper.IsInstallFraemwork)
                {
                    OnChangedStatus("Install .NET Framework 4.7.2 ...");
                    DriversHelper.InstallFraemwork();
                }

                if (!DriversHelper.IsInstallFlyCapture)
                {
                    OnChangedStatus("Install FlyCapture ...");
                    DriversHelper.InstallFlyCapture();
                }
            }
            

            //if (!DriversHelper.IsInstallDirectx())
            //    DriversHelper.InstallDirectx();
            OnFinashed();
        }

        /// <summary>
        /// Установка ини файла конфигурации.
        /// </summary>
        private static void InstallIniFile()
        {
            if (!File.Exists(Settings.IniFilePath))
            {
                Directory.CreateDirectory(Settings.ProgramDataAppFolder);
                using (var stream = new FileStream(Settings.IniFilePath, FileMode.CreateNew))
                {
                    var data = Encoding.UTF8.GetBytes(Resources.config);
                    stream.Write(data, 0, data.Length);
                }
            }
        }

        /// <summary>
        /// Установка путей в ini файл.
        /// </summary>
        private static void ChangeIniFile()
        {
            var iniFile = new IniFile(Settings.IniFilePath);
            iniFile.IniWriteValue("DataBase", "DB_Path", $@"{Setup.SmallDbFolderPath}\test.db");
            iniFile.IniWriteValue("DataBase", "DB_VideoPath", $@"{Setup.BigDbFolderPath}");
        }

        /// <summary>
        /// Удаление файла конфигурации.
        /// </summary>
        private static void UninstallIni()
        {
            if(File.Exists(Settings.IniFilePath))
                File.Delete(Settings.IniFilePath);

            // Если папка пустая, удаляем.
            if(Directory.Exists(Settings.IniFilePath))
                if(!Directory.EnumerateFileSystemEntries(Settings.ProgramDataAppFolder).Any())
                    Directory.Delete(Settings.ProgramDataAppFolder);

            if (Directory.Exists(Settings.ProgramDataManufacturerFolder))
                if (!Directory.EnumerateFileSystemEntries(Settings.ProgramDataManufacturerFolder).Any())
                    Directory.Delete(Settings.ProgramDataManufacturerFolder);
        }

        /// <summary>
        /// Запустить после установки.
        /// </summary>
        public static void RunExeAfterInstall()
        {
            var exeFilePath = Path.Combine(Setup.AppFolderPath, Settings.MainExeFileName);
            if (File.Exists(exeFilePath))
            {
                System.Diagnostics.Process.Start(exeFilePath);
            }
        }

        /// <summary>
        /// Оповещает когда закончил работу.
        /// </summary>
        private static void OnFinashed()
        {
            Finished?.Invoke(null, EventArgs.Empty);
        }

        /// <summary>
        /// Оповещает чем занят класс.
        /// </summary>
        private static void OnChangedStatus(string e)
        {
            ChangedStatus?.Invoke(null, e);
        }
    }
}

﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace WpfSetup.Mvvm.Converters
{
    public class BoolToImageSourceConverter : IValueConverter
    {
        public string TrueImageSource = "../../Image/Yes.png";
        public string FalseImageSource = "../../Image/No.png";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool) value ? TrueImageSource : FalseImageSource;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

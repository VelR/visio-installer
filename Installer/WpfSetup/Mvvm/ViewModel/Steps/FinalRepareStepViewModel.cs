﻿using System;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class FinalRepareStepViewModel : BaseStepViewModel
    {
        public FinalRepareStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveNextStep = true;
            HavePreviewStep = true;
            CanPreviewStep = true;
        }
    }
}

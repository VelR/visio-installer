﻿namespace WpfSetup.Mvvm.ViewModel.Steps
{
    /// <summary>
    /// VM шага установки отображающий процесс установки.
    /// </summary>
    public class ProgressStepViewModel: BaseStepViewModel
    {
        public ProgressStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveCancel = true;
        }
    }
}

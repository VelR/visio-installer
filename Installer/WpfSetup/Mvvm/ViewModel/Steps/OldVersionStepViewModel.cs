﻿namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class OldVersionStepViewModel : BaseStepViewModel
    {
        public OldVersionStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveFinalStep = true;
            CanFinalStep = true;
        }
    }
}

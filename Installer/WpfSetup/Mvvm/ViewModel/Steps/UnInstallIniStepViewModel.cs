﻿using System;
using System.Windows;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class UnInstallIniStepViewModel : BaseStepViewModel
    {
        public bool IsSaveIniFile
        {
            get => (bool)GetValue(IsSaveIniFileProperty);
            set => SetValue(IsSaveIniFileProperty, value);
        }

        public static readonly DependencyProperty IsSaveIniFileProperty =
            DependencyProperty.Register(nameof(IsSaveIniFile), typeof(bool), typeof(UnInstallIniStepViewModel), new PropertyMetadata(false, IsSaveIniFileChanged));

        private static void IsSaveIniFileChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var viewModel = (UnInstallIniStepViewModel)d;
            viewModel.Setup.IsDeleteIni = !(bool) e.NewValue;
        }

        public UnInstallIniStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveUninstall = true;
            CanUninstall = true;

            HavePreviewStep = true;
            CanPreviewStep = true;
        }
    }
}

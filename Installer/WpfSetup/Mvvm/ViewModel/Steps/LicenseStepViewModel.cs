﻿using System;
using System.Windows;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class LicenseStepViewModel : BaseStepViewModel
    {
        public bool IsApplyLicense
        {
            get { return (bool)GetValue(IsApplyLicenseProperty); }
            set { SetValue(IsApplyLicenseProperty, value); }
        }

        public static readonly DependencyProperty IsApplyLicenseProperty =
            DependencyProperty.Register(nameof(IsApplyLicense), typeof(bool), typeof(LicenseStepViewModel), new PropertyMetadata(false, IsApplyChanged));

        private static void IsApplyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var viewModel = (LicenseStepViewModel)d;
            viewModel.CanNextStep = (bool)e.NewValue;
        }

        public LicenseStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveNextStep = true;
            HavePreviewStep = true;
            CanPreviewStep = true;
        }
    }
}

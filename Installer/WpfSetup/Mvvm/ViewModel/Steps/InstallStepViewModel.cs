﻿using System.Windows;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class InstallStepViewModel : BaseStepViewModel
    {
        #region Свойства

        /// <summary>
        /// Добваить ярлык на рабочий стол.
        /// </summary>
        public bool IsAddDesktop
        {
            get => (bool)GetValue(IsAddDesktopProperty);
            set => SetValue(IsAddDesktopProperty, value);
        }

        public static readonly DependencyProperty IsAddDesktopProperty =
            DependencyProperty.Register(nameof(IsAddDesktop), typeof(bool), typeof(InstallStepViewModel), new PropertyMetadata(false, 
                (o, args) => App.Setup.IsAddShortcutDesktop = (bool)args.NewValue));


        /// <summary>
        /// Добваить ярлык в пуск.
        /// </summary>
        public bool IsAddProgramm
        {
            get => (bool)GetValue(IsAddProgrammProperty);
            set => SetValue(IsAddProgrammProperty, value);
        }

        public static readonly DependencyProperty IsAddProgrammProperty =
            DependencyProperty.Register(nameof(IsAddProgramm), typeof(bool), typeof(InstallStepViewModel), new PropertyMetadata(false, 
                (o, args) => App.Setup.IsAddShortcutProgram = (bool)args.NewValue));

        #endregion

        public InstallStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {            
            HavePreviewStep = true;
            CanPreviewStep = true;

            HaveInstall = true;
            CanInstall = true;
        }
    }
}

﻿using System.IO;
using System.Windows;
using System.Windows.Forms;
using CommonLib;
using WpfSetup.Common;
using WpfSetup.Mvvm.Common;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    /// <summary>
    /// Шаг отвечает за выбор папок для установки.
    /// </summary>
    public class FolderAppStepViewModel : BaseStepViewModel
    {
        #region Свойства

        /// <summary>
        /// Можно ли нажать далее.
        /// </summary>
        public override bool CanNextStep => Setup.IsEnoughSpace;

        /// <summary>
        /// Путь установки приложения.
        /// </summary>
        public string AppFolderPath
        {
            get { return (string)GetValue(AppFolderPathProperty); }
            set { SetValue(AppFolderPathProperty, value); }
        }

        public static readonly DependencyProperty AppFolderPathProperty =
            DependencyProperty.Register(nameof(AppFolderPath), typeof(string), typeof(FolderAppStepViewModel), new PropertyMetadata(string.Empty, AppFolderChanged));

        private static void AppFolderChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var viewModel = (FolderAppStepViewModel)d;
            viewModel.Setup.AppFolderPath = (string)e.NewValue;
            viewModel.IsNotEnoughAppSpace = !SizeHelper.CheckFreeSpace(viewModel.Setup.AppFolderPath, Settings.RequiredSize);
        }

        /// <summary>
        /// Путь установки крупной базы.
        /// </summary>
        public string BigDbFolderPath
        {
            get { return (string)GetValue(BigDbFolderPathProperty); }
            set { SetValue(BigDbFolderPathProperty, value); }
        }

        public static readonly DependencyProperty BigDbFolderPathProperty =
            DependencyProperty.Register(nameof(BigDbFolderPath), typeof(string), typeof(FolderAppStepViewModel), new PropertyMetadata(string.Empty, BigDpFolderChanged));

        private static void BigDpFolderChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var viewModel = (FolderAppStepViewModel)d;
            viewModel.Setup.BigDbFolderPath = (string)e.NewValue;
            viewModel.IsNotEnoughBigDbSpace = !SizeHelper.CheckFreeSpace(viewModel.Setup.AppFolderPath, Settings.RequiredBigDbSize);
        }

        /// <summary>
        /// Путь установки малой базы.
        /// </summary>
        public string SmallDbFolderPath
        {
            get { return (string)GetValue(SmallDbFolderPathProperty); }
            set { SetValue(SmallDbFolderPathProperty, value); }
        }

        public static readonly DependencyProperty SmallDbFolderPathProperty =
            DependencyProperty.Register(nameof(SmallDbFolderPath), typeof(string), typeof(FolderAppStepViewModel), new PropertyMetadata(string.Empty, (o, args) => App.Setup.SmallDbFolderPath = (string)args.NewValue));

        /// <summary>
        /// Достаточно ли свободного места для приложения.
        /// </summary>
        public bool IsNotEnoughAppSpace
        {
            get => (bool)GetValue(IsEnIsEnoughAppSpaceProperty);
            set => SetValue(IsEnIsEnoughAppSpaceProperty, value);
        }

        public static readonly DependencyProperty IsEnIsEnoughAppSpaceProperty =
            DependencyProperty.Register(nameof(IsNotEnoughAppSpace), typeof(bool), typeof(FolderAppStepViewModel), new PropertyMetadata(false, (o, args) => App.Setup.IsNotEnoughAppSpace = !(bool)args.NewValue));


        /// <summary>
        /// Достаточно ли свободного места для большой базы.
        /// </summary>
        public bool IsNotEnoughBigDbSpace
        {
            get => (bool)GetValue(IsNotEnoughBigDbSpaceProperty);
            set => SetValue(IsNotEnoughBigDbSpaceProperty, value);
        }

        public static readonly DependencyProperty IsNotEnoughBigDbSpaceProperty =
            DependencyProperty.Register(nameof(IsNotEnoughBigDbSpace), typeof(bool), typeof(FolderAppStepViewModel), new PropertyMetadata(false, (o, args) => App.Setup.IsNotEnoughBigDbSpace = !(bool)args.NewValue));

        #endregion

        /// <summary>
        /// Шаг отвечает за выбор папок для установки.
        /// </summary>
        public FolderAppStepViewModel()
        {
            AppFolderPath = Setup.AppFolderPath;

            var iniFile = new IniFile(Settings.IniFilePath);
            var dbPath = iniFile.IniReadValue("DataBase", "DB_Path");
            var dbVideoPath = iniFile.IniReadValue("DataBase", "DB_VideoPath");

            if (!string.IsNullOrEmpty(dbPath))
            {
                dbPath = Path.GetDirectoryName(dbPath);
            }

            BigDbFolderPath = string.IsNullOrEmpty(dbVideoPath) ? Setup.BigDbFolderPath : dbVideoPath;
            SmallDbFolderPath = string.IsNullOrEmpty(dbPath) ? Setup.SmallDbFolderPath : dbPath;

            InitSteps();
        }

        private void InitSteps()
        {
            HaveNextStep = true;
            CanNextStep = true;
            HavePreviewStep = true;
            CanPreviewStep = true;
        }

        #region Методы



        #endregion

        #region Комманды

        /// <summary>
        /// Команда смены директории основного приложения.
        /// </summary>
        public RelayCommand ChangeAppDirCommand =>
            new RelayCommand(obj =>
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    fbd.SelectedPath = AppFolderPath;
                    var result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        AppFolderPath = fbd.SelectedPath;
                        if (!AppFolderPath.ToUpper().Contains($@"{Settings.Manufacturer}\{Settings.ApplicationName}"))
                        {
                            AppFolderPath = Path.Combine(AppFolderPath, Settings.Manufacturer, Settings.ApplicationName);
                        }
                    }
                }
            });

        /// <summary>
        /// Команда смены директории большой БД.
        /// </summary>
        public RelayCommand ChangeBigDbCommand =>
            new RelayCommand(obj =>
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    fbd.SelectedPath = BigDbFolderPath;
                    var result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        BigDbFolderPath = fbd.SelectedPath;
                    }
                }
            });

        /// <summary>
        /// Команда смены директории бж.
        /// </summary>
        public RelayCommand ChangeSmallDbCommand =>
            new RelayCommand(obj =>
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    fbd.SelectedPath = SmallDbFolderPath;
                    var result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        SmallDbFolderPath = fbd.SelectedPath;
                    }
                }
            });

        #endregion
    }
}

﻿using System;
using WpfSetup.Localization;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class FinalUnInstallStepViewModel : BaseStepViewModel
    {
        public FinalUnInstallStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveFinalStep = true;
        }
    }
}

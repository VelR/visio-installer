﻿namespace WpfSetup.Mvvm.ViewModel.Steps
{
    /// <summary>
    /// Шаг окончания загрузки.
    /// </summary>
    public class FinalInstallStepViewModel : BaseStepViewModel
    {
        /// <summary>
        /// Успешная ли была установка.
        /// </summary>
        public bool IsSuccsfullInstallation => !Setup.CancelRequested;

        public FinalInstallStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveFinalStep = true;
        }
    }
}

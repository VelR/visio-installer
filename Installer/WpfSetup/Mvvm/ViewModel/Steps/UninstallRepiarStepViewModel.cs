﻿using System.Windows;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class UninstallRepiarStepViewModel : BaseStepViewModel
    {
        public bool IsUninstall
        {
            get => (bool)GetValue(IsUninstallProperty);
            set => SetValue(IsUninstallProperty, value);
        }

        public static readonly DependencyProperty IsUninstallProperty =
            DependencyProperty.Register(nameof(IsUninstall), typeof(bool), typeof(UninstallRepiarStepViewModel), new PropertyMetadata(true,
                (o, args) =>
                {
                    var newValue = (bool) args.NewValue;
                    var viewmodel = (UninstallRepiarStepViewModel)o;
                    viewmodel.HaveNextStep = newValue;
                    viewmodel.HaveRepare = !newValue;
                }));

        
        public UninstallRepiarStepViewModel()
        {
            InitSteps();
        }

        public override void NextStep()
        {
            if (IsUninstall)
            {
                Setup.SetupComplete += AfterInstallAction.RunAfterInstallAction;
                App.MainWindowViewModel.SetupStepViewModels.AddLast(new UnInstallIniStepViewModel());
                App.MainWindowViewModel.SetupStepViewModels.AddLast(new ProgressStepViewModel());
                App.MainWindowViewModel.SetupStepViewModels.AddLast(new FinalUnInstallStepViewModel());
            }
        }

        private void InitSteps()
        {
            HaveNextStep = true;
            CanNextStep = true;
        }
    }
}

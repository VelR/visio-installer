﻿using System;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class ShortcutsStepViewModel : BaseStepViewModel
    {
        public ShortcutsStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveNextStep = true;
            CanNextStep = true;
            HavePreviewStep = true;
            CanPreviewStep = true;
        }
    }
}

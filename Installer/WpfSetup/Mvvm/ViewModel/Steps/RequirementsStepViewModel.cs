﻿using CommonLib;
using WpfSetup.Mvvm.Common;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class RequirementsStepViewModel : BaseStepViewModel
    {
        #region Свойства

        public bool IsCpuValid { get; set; }
        public bool IsGpuValid { get; set; }
        public bool IsRamValid { get; set; }
        public bool IsAppFreeSpaceValid { get; set; }
        public bool IsBigDbFreeSpaceValid { get; set; }

        #endregion

        public RequirementsStepViewModel()
        {
            InitSteps();
        }

        /// <summary>
        /// Вызывается выполняется переход к шагу.
        /// </summary>
        public override void StepStart()
        {
            Analise();
        }

        /// <summary>
        /// Произвести анализ минимальных системных требований.
        /// </summary>
        private void Analise()
        {
            IsCpuValid = CheckCpu();
            IsGpuValid = CheckGpu();
            IsRamValid = CheckRam();
            IsAppFreeSpaceValid = CheckAppSpace();
            IsBigDbFreeSpaceValid = CheckBdSpace();
        }

        #region Методы

        private void InitSteps()
        {
            HaveNextStep = true;
            CanNextStep = true;
            HavePreviewStep = true;
            CanPreviewStep = true;
        }

        /// <summary>
        /// Проверка на наличие свободного места для большой базы данных.
        /// </summary>
        private bool CheckBdSpace()
        {
            return AnaliseHelper.CheckFreeSpace(Setup.BigDbFolderPath, Settings.RequiredBigDbSize);
        }

        /// <summary>
        /// Проверка на наличие свободного места приложению.
        /// </summary>
        private bool CheckAppSpace()
        {
            return AnaliseHelper.CheckFreeSpace(Setup.AppFolderPath, Settings.RequiredSize);
        }

        /// <summary>
        /// Проверка ОЗУ.
        /// </summary>
        private bool CheckRam()
        {
            return AnaliseHelper.CheckRam();
        }

        /// <summary>
        /// Проверка видеокарты.
        /// </summary>
        private bool CheckGpu()
        {
            return AnaliseHelper.CheckGpu();
        }

        /// <summary>
        /// Проверка процессора.
        /// </summary>
        private bool CheckCpu()
        {
            return AnaliseHelper.CheckCpu();
        }

        #endregion
    }
}

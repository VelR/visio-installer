﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class DriverProgressStepViewModel : BaseStepViewModel
    {
        /// <summary>
        /// Статус установки доп ресурсов.
        /// </summary>
        public string StatusText
        {
            get => (string)GetValue(StatusTextProperty);
            set => SetValue(StatusTextProperty, value);
        }

        public static readonly DependencyProperty StatusTextProperty =
            DependencyProperty.Register(nameof(StatusText), typeof(string), typeof(DriverProgressStepViewModel), new PropertyMetadata(null));


        /// <summary>
        /// Вызывается при переходе к шагу.
        /// </summary>
        public override void StepStart()
        {
            AfterInstallAction.ChangedStatus += (sender, s) =>
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        StatusText = s;
                    }));
                };
            AfterInstallAction.Finished += (sender, s) => Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(OnFinished));
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                new Thread(AfterInstallAction.RunAfterInstallAction).Start();
            }));
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using WpfSetup.Localization;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    public class HelloStepViewModel : BaseStepViewModel
    {
        /// <summary>
        /// Применяемые языки.
        /// </summary>
        private static readonly IReadOnlyCollection<CultureInfo> Languages = new Collection<CultureInfo>()
        {
            new CultureInfo("ru-RU"),
            new CultureInfo("en-US"),
        };

        /// <summary>
        /// Языки для установки.
        /// </summary>
        public IReadOnlyCollection<CultureInfo> LanguagesSourse
        {
            get => (IReadOnlyCollection<CultureInfo>)GetValue(LanguagesSourseProperty);
            set => SetValue(LanguagesSourseProperty, value);
        }

        public static readonly DependencyProperty LanguagesSourseProperty =
            DependencyProperty.Register(nameof(LanguagesSourse), typeof(IReadOnlyCollection<CultureInfo>), typeof(HelloStepViewModel), new PropertyMetadata(Languages));

        /// <summary>
        /// Выбранный язык.
        /// </summary>
        public CultureInfo SelectedLanguage
        {
            get => (CultureInfo)GetValue(SelectedLanguageProperty);
            set => SetValue(SelectedLanguageProperty, value);
        }

        public static readonly DependencyProperty SelectedLanguageProperty =
            DependencyProperty.Register(nameof(SelectedLanguage), typeof(CultureInfo), typeof(HelloStepViewModel), new PropertyMetadata(Languages.First(x => Equals(x, TranslationSource.Instance.CurrentCulture)),
                (o, args) =>
                {
                    TranslationSource.Instance.CurrentCulture = (CultureInfo)args.NewValue;
                }));


        public HelloStepViewModel()
        {
            InitSteps();
        }

        private void InitSteps()
        {
            HaveNextStep = true;
            CanNextStep = true;
        }
    }
}

﻿using System;
using System.Windows;

namespace WpfSetup.Mvvm.ViewModel.Steps
{
    /// <summary>
    /// Базовый класс отвечающий за наличие кнопок у шагов установки.
    /// </summary>
    public class BaseStepViewModel : DependencyObject
    {
        /// <summary>
        /// События окончания работы шага.
        /// </summary>
        public event EventHandler Finished;

        /// <summary>
        /// Вызывается при переходе к шагу.
        /// </summary>
        public virtual void StepStart() { }

        /// <summary>
        /// Вызывается перед переходом к следующему шагу.
        /// </summary>
        public virtual void NextStep() { }

        /// <summary>
        /// Доступ к Setup из VM шагов.
        /// </summary>
        public VisionSetup Setup => App.Setup;

        #region Состояния кнопок для шагов

        /// <summary>
        /// Можно ли нажать кнопку "Далее".
        /// </summary>
        public virtual bool CanNextStep { get; set; }

        /// <summary>
        /// Видна ли кнопка "Далее".
        /// </summary>
        public bool HaveNextStep
        {
            get => (bool)GetValue(HaveNextStepProperty);
            set => SetValue(HaveNextStepProperty, value);
        }

        public static readonly DependencyProperty HaveNextStepProperty =
            DependencyProperty.Register(nameof(HaveNextStep), typeof(bool), typeof(BaseStepViewModel), new PropertyMetadata(false));


        /// <summary>
        /// Можно ли нажать кнопку "Назад".
        /// </summary>
        public bool CanPreviewStep { get; set; }

        /// <summary>
        /// Видна ли кнопка "Назад".
        /// </summary>
        public bool HavePreviewStep
        {
            get => (bool)GetValue(HavePreviewStepProperty);
            set => SetValue(HavePreviewStepProperty, value);
        }

        public static readonly DependencyProperty HavePreviewStepProperty =
            DependencyProperty.Register(nameof(HavePreviewStep), typeof(bool), typeof(BaseStepViewModel), new PropertyMetadata(false));

        /// <summary>
        /// Можно ли нажать кнопку "Отмена".
        /// </summary>
        //public bool CanCancel { get; set; }

        /// <summary>
        /// Видна ли кнопка "Отмена".
        /// </summary>
        public bool HaveCancel
        {
            get => (bool)GetValue(HaveCancelProperty);
            set => SetValue(HaveCancelProperty, value);
        }

        public static readonly DependencyProperty HaveCancelProperty =
            DependencyProperty.Register(nameof(HaveCancel), typeof(bool), typeof(BaseStepViewModel), new PropertyMetadata(false));

        /// <summary>
        /// Можно ли нажать кнопку "Установить".
        /// </summary>
        public bool CanInstall { get; set; }

        /// <summary>
        /// Видна ли кнопка "Установить".
        /// </summary>
        public bool HaveInstall
        {
            get => (bool)GetValue(HaveInstallProperty);
            set => SetValue(HaveInstallProperty, value);
        }

        public static readonly DependencyProperty HaveInstallProperty =
            DependencyProperty.Register(nameof(HaveInstall), typeof(bool), typeof(BaseStepViewModel), new PropertyMetadata(false));


        /// <summary>
        /// Можно ли нажать кнопку "Восстановить".
        /// </summary>
        public bool CanRepare { get; set; }

        /// <summary>
        /// Видна ли кнопка "Восстановить".
        /// </summary>
        public bool HaveRepare
        {
            get => (bool)GetValue(HaveRepareProperty);
            set => SetValue(HaveRepareProperty, value);
        }

        public static readonly DependencyProperty HaveRepareProperty =
            DependencyProperty.Register(nameof(HaveRepare), typeof(bool), typeof(BaseStepViewModel), new PropertyMetadata(false));


        /// <summary>
        /// Можно ли нажать кнопку "Удалить".
        /// </summary>
        public bool CanUninstall { get; set; }

        /// <summary>
        /// Видна ли кнопка "Удалить".
        /// </summary>
        public bool HaveUninstall
        {
            get => (bool)GetValue(HaveUninstallProperty);
            set => SetValue(HaveUninstallProperty, value);
        }

        public static readonly DependencyProperty HaveUninstallProperty =
            DependencyProperty.Register(nameof(HaveUninstall), typeof(bool), typeof(BaseStepViewModel), new PropertyMetadata(false));


        /// <summary>
        /// Можно ли нажать кнопку "Завершить".
        /// </summary>
        public bool CanFinalStep { get; set; }

        /// <summary>
        /// Видна ли кнопка "Завершить".
        /// </summary>
        public bool HaveFinalStep
        {
            get => (bool)GetValue(HaveFinalStepProperty);
            set => SetValue(HaveFinalStepProperty, value);
        }

        public static readonly DependencyProperty HaveFinalStepProperty =
            DependencyProperty.Register(nameof(HaveFinalStep), typeof(bool), typeof(BaseStepViewModel), new PropertyMetadata(false));

        #endregion

        /// <summary>
        /// Обработчик события оповещения окончания степа.
        /// </summary>
        public virtual void OnFinished()
        {
            Finished?.Invoke(this, EventArgs.Empty);
        }
    }
}

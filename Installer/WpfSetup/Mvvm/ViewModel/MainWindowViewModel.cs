﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using WpfSetup.Mvvm.Common;
using WpfSetup.Mvvm.ViewModel.Steps;

namespace WpfSetup.Mvvm.ViewModel
{
    public class MainWindowViewModel : DependencyObject
    {
        /// <summary>
        /// Установочник.
        /// </summary>
        public VisionSetup Setup => App.Setup;

        /// <summary>
        /// Шаги установочника.
        /// </summary>
        public LinkedList<BaseStepViewModel> SetupStepViewModels { get; } = new LinkedList<BaseStepViewModel>();

        /// <summary>
        /// Отвечает за то какое шаг показывает окно.
        /// </summary>
        public LinkedListNode<BaseStepViewModel> SelectedStepViewModel
        {
            get => (LinkedListNode<BaseStepViewModel>)GetValue(SelectedStepViewModelProperty);
            set => SetValue(SelectedStepViewModelProperty, value);
        }

        public static readonly DependencyProperty SelectedStepViewModelProperty =
            DependencyProperty.Register(nameof(SelectedStepViewModel), typeof(LinkedListNode<BaseStepViewModel>), typeof(MainWindowViewModel), new PropertyMetadata(null));


        public MainWindowViewModel()
        {
            Setup.SetupComplete += SetupOnSetupComplete;

            // Если на пк установлена более свежая версия:
            if (Setup.IsOldVersion)
            {
                SetupStepViewModels.AddLast(new OldVersionStepViewModel());
            }
            else
            {
                if (Setup.CanInstall)
                {
                    SetupStepViewModels.AddLast(new HelloStepViewModel());
                    SetupStepViewModels.AddLast(new LicenseStepViewModel());
                    SetupStepViewModels.AddLast(new FolderAppStepViewModel());
                    SetupStepViewModels.AddLast(new RequirementsStepViewModel());
                    SetupStepViewModels.AddLast(new InstallStepViewModel());
                    SetupStepViewModels.AddLast(new ProgressStepViewModel());

                    var driverProgressStepViewModel = new DriverProgressStepViewModel();
                    driverProgressStepViewModel.Finished += (sender, args) => NextStep();
                    SetupStepViewModels.AddLast(driverProgressStepViewModel);

                    SetupStepViewModels.AddLast(new FinalInstallStepViewModel());
                }

                if (Setup.CanUnInstall)
                {
                    SetupStepViewModels.AddLast(new UninstallRepiarStepViewModel());
                }
            }

            SelectedStepViewModel = SetupStepViewModels.First;
        }

        /// <summary>
        /// Обработка события окончания установки.
        /// </summary>
        private void SetupOnSetupComplete()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(NextStep));
        }

        public void WindowOnClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = Setup.IsRunning;
        }

        /// <summary>
        /// Переход к слежующему шагу.
        /// </summary>
        private void NextStep()
        {
            SelectedStepViewModel.Value.NextStep();
            SelectedStepViewModel = SelectedStepViewModel.Next;
            SelectedStepViewModel.Value.StepStart();
        }
        #region Комманды

        /// <summary>
        /// Команда запуска установки.
        /// </summary>
        public RelayCommand InstallCommand =>
            new RelayCommand(obj =>
            {
                NextStep();
                Setup.StartInstall();                
            }, o => Setup.CanInstall);

        /// <summary>
        /// Команда запуска переустановки.
        /// </summary>
        public RelayCommand RepareCommand =>
            new RelayCommand(obj =>
            {
                SetupStepViewModels.AddLast(new ProgressStepViewModel());
                SetupStepViewModels.AddLast(new FinalInstallStepViewModel());
                Setup.StartRepair();
                NextStep();
            }, o => Setup.CanRepair);

        /// <summary>
        /// Команда запуска удаления.
        /// </summary>
        public RelayCommand UninstallCommand =>
            new RelayCommand(obj =>
            {
                Setup.StartUninstall();
                NextStep();
            }, o => Setup.CanUnInstall);

        /// <summary>
        /// Команда отмены.
        /// </summary>
        public RelayCommand CancelCommand =>
            new RelayCommand(obj =>
            {
                Setup.CancelRequested = true;
            }, o => Setup.IsRunning);

        /// <summary>
        /// Команда седующего шага.
        /// </summary>
        public RelayCommand NextStepCommand =>
            new RelayCommand(obj =>
            {
                NextStep();
            }, o => SelectedStepViewModel.Value.CanNextStep);        

        /// <summary>
        /// Команда предыдущего шага.
        /// </summary>
        public RelayCommand PreviewStepCommand =>
            new RelayCommand(obj =>
            {
                SelectedStepViewModel = SelectedStepViewModel.Previous;
            }, o => SelectedStepViewModel.Value.CanPreviewStep);

        /// <summary>
        /// Команда заключительного шага.
        /// </summary>
        public RelayCommand FinalStepCommand =>
            new RelayCommand(obj =>
            {
                if (Setup.IsRunApp)
                {
                    AfterInstallAction.RunExeAfterInstall();
                }
                Environment.Exit(0);
            });

        #endregion
    }
}

﻿using System.Windows;

namespace WpfSetup.Mvvm.View
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void WindowOnLoaded(object sender, RoutedEventArgs e)
        {
            App.HideSplashScreen();
        }

        private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }

        //private void Close_Click(object sender, RoutedEventArgs e)
        //{
        //    Close();
        //}

        //private void ShowLog_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    //Process.Start(Setup.LogFile);
        //}
    }
}
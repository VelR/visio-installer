﻿using System.IO;

namespace WpfSetup.Mvvm.Common
{
    /// <summary>
    /// Класс предназначен для определения свободного места на диске.
    /// </summary>
    public static class SizeHelper
    {
        /// <summary>
        /// Определяет свободное место на диске.
        /// </summary>
        private static long GetFreeDiscSpace(string path)
        {
            var driveName = Path.GetPathRoot(path);
            var drive = new DriveInfo(driveName);
            var size = drive.TotalFreeSpace;
            return size;
        }

        /// <summary>
        /// Проверяет хватает ли свободного места на диске.
        /// </summary>
        public static bool CheckFreeSpace(string path, string requiredSize)
        {
            var size = (long)(double.Parse(requiredSize) * 1024 * 1024 * 1024);
            var freeSpace = GetFreeDiscSpace(path);
            return size < freeSpace;
        }
    }
}

﻿using System;
using System.Management;
using CommonLib;
using WpfSetup.Common;

namespace WpfSetup.Mvvm.Common
{
    /// <summary>
    /// Класс предназначин для анализа минимальных системных требований.
    /// </summary>
    public static class AnaliseHelper
    {
        /// <summary>
        /// Проверка на наличие свободного места.
        /// </summary>
        /// <param name="path">Путь установки</param>
        /// <param name="size">Размер устанавливаемого пакета</param>
        public static bool CheckFreeSpace(string path, string size)
        {
            return SizeHelper.CheckFreeSpace(path, size);
        }

        /// <summary>
        /// Проверка ОЗУ.
        /// </summary>
        public static bool CheckRam()
        {
            var ramMonitor = new ManagementObjectSearcher("SELECT TotalVisibleMemorySize FROM Win32_OperatingSystem");

            ulong totalRam = 0;

            foreach (ManagementObject obj in ramMonitor.Get())
            {
                totalRam = Convert.ToUInt64(obj["TotalVisibleMemorySize"])/1024/1024;
            }

            return Convert.ToUInt64(Settings.TotalRam) <= (totalRam + 1);
        }

        /// <summary>
        /// Проверка видеокарты.
        /// </summary>
        public static bool CheckGpu()
        {
            var gpuMonitor = new ManagementObjectSearcher("SELECT Description FROM win32_videocontroller");

            foreach (ManagementObject obj in gpuMonitor.Get())
            {
                // Примерный вид NVIDIA GeForce 7600 GT, 
                var description = ((string)obj["Description"]).Split(' ');

                // Если не NVIDIA
                if (description[0].ToUpper()!= "NVIDIA") return false;

                // Если младше 7 поколения
                int.TryParse(description[2][0].ToString(), out var numberGen);
                if (numberGen < 7) return false;
            }

            return true;
        }

        /// <summary>
        /// Проверка процессора.
        /// </summary>
        public static bool CheckCpu()
        {
            var cpuMonitor = new ManagementObjectSearcher("SELECT Name, Manufacturer FROM win32_processor");

            foreach (ManagementObject obj in cpuMonitor.Get())
            {
                // Если не Intel
                if (!((string) obj["Manufacturer"]).ToLower().Contains("intel")) return false;

                // Name имеет вид: "Intel<R> Core<R> i*-**** CPU ..."
                var generation = ((string)obj["Name"]).Split(' ')[2]; // Вытаскиваем строку поколения

                // Если младше i5
                if (generation[1] < '5') return false; // минимальный i5

                // Если младше 3-го поколения
                if (generation[3] < '3') return false; // минимальный i5-3***** (Ivy Bridge)
            }

            return true;
        }
    }
}

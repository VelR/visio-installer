using System.Linq;
using WindowsInstaller;
using CommonLib;
using Microsoft.Win32;
using WixSharp.UI;

namespace WpfSetup
{
    /// <summary>
    /// ������������.
    /// </summary>
    public class VisionSetup : GenericSetup
    {
        #region ��������
        
        /// <summary>
        /// ���� ��������� ����������.
        /// </summary>
        public string AppFolderPath { get; set; } = Settings.ApplicationPath;

        /// <summary>
        /// ���� ��������� ������� ���� ������.
        /// </summary>
        public string BigDbFolderPath { get; set; } = Settings.BigDbPath;

        /// <summary>
        /// ���� ��������� ����� ���� ������.
        /// </summary>
        public string SmallDbFolderPath { get; set; } = Settings.SmallDbPath;

        /// <summary>
        /// ���� ������, ���� ������������ ���������� ����� ��� ����������.
        /// </summary>
        public bool IsNotEnoughAppSpace { get; set; }

        /// <summary>
        /// ���� ������, ���� ������������ ���������� ����� ��� ��.
        /// </summary>
        public bool IsNotEnoughBigDbSpace { get; set; }

        /// <summary>
        /// ���� ������, ���� ���������� ���������� �����.
        /// </summary>
        public bool IsEnoughSpace => !IsNotEnoughAppSpace; //!IsNotEnoughAppSpace && !IsEnoughBigDbSpace;

        /// <summary>
        /// ������� ����� �� ������� ����.
        /// </summary>
        public bool IsAddShortcutDesktop { get; set; }

        /// <summary>
        /// ������� ����� � ����.
        /// </summary>
        public bool IsAddShortcutProgram { get; set; }

        /// <summary>
        /// ��������� ���������� ����� ���������.
        /// </summary>
        public bool IsRunApp { get; set; }

        /// <summary>
        /// ������� �� ����� ��������.
        /// </summary>
        public bool IsUninstall { get; set; }

        /// <summary>
        /// ���� �� ����������.
        /// </summary>
        public string FraemworkExePath { get; set; }

        /// <summary>
        /// ���� �� FlyCapture.
        /// </summary>
        public string FlyCaptureExePath { get; set; }

        /// <summary>
        /// ���� �� Directx.
        /// </summary>
        public string DirectxExePath { get; set; }

        /// <summary>
        /// ������� ���� ������������.
        /// </summary>
        public bool IsDeleteIni { get; set; } = true;

        /// <summary>
        /// �������� �� ������ ������ ������������� ������ ��� ������ �������������� ��.
        /// </summary>
        public bool IsOldVersion
        {
            get
            {
                var thisOldVersion = false;
                var installedVersion = (string)Registry.GetValue($@"HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\{Settings.Manufacturer}\{Settings.ApplicationName}", "Version", null);
                if (!string.IsNullOrWhiteSpace(installedVersion))
                {
                    if (ProductVersion != installedVersion)
                    {
                        var installedVersionArr = installedVersion.Split('.').Select(int.Parse).ToList();
                        var thisVersionArr = ProductVersion.Split('.').Select(int.Parse).ToList();

                        // ��������� ����������� �� ����� ������ ���������.
                        if (installedVersionArr.Where((t, i) => t > thisVersionArr[i]).Any())
                        {
                            thisOldVersion = true;
                        }
                    }
                }
                return thisOldVersion;
            }
        }

        #endregion

        #region ����������

        /// <summary>
        /// ������������.
        /// </summary>
        /// <param name="msiFile">���� ���������.</param>
        /// <param name="enableLoging">�������� �� �����������.</param>
        public VisionSetup(string msiFile, bool enableLoging = true) : base(msiFile, enableLoging)
        {
            SetupStarted += VisionOnSetupStarted;
        }
        

        #endregion

        #region ������

        /// <summary>
        /// ��������� ������ ���������.
        /// </summary>
        public override void OnError(string data, bool fatal, MsiInstallMessage? relatedMessageType = null)
        {
            base.OnError(data, fatal, relatedMessageType);
        }

        /// <summary>
        /// ������ �������.
        /// </summary>
        public void StartRepair()
        {
            //The MSI will abort any attempt to start unless CUSTOM_UI is set. This  a feature for preventing starting the MSI without this custom GUI.
            base.StartRepair("CUSTOM_UI=true");
        }

        ///// <summary>
        ///// ������ ���������.
        ///// </summary>
        //public void StartChange()
        //{
        //    // ����� ������� ����� ������� ��� ����.
        //    // ��������� ����� �����������.
        //    var addLocal = $"ADDLOCAL={Features.Application}";

        //    base.StartRepair($"CUSTOM_UI=true {addLocal} {Props.InstallDir}=\"{AppFolderPath}\" {Props.InstallDesktopShortcut}=\"{IsAddShortcutDesktop}\" {Props.InstallStartprogramShortcut}=\"{IsAddShortcutProgram}\"");
        //}

        /// <summary>
        /// ������ ��������.
        /// </summary>
        public void StartUninstall()
        {
            IsUninstall = true;
            base.StartUninstall();
        }

        /// <summary>
        /// ������ ���������.
        /// </summary>
        public void StartInstall()
        {
            // ����� ������� ����� ������� ��� ����.
            // ��������� ����� �����������.
            var addLocal = $"ADDLOCAL={Features.Application}";

            base.StartInstall($"CUSTOM_UI=true {addLocal} {Props.InstallDir}=\"{AppFolderPath}\" {Props.InstallDesktopShortcut}=\"{IsAddShortcutDesktop}\" {Props.InstallStartprogramShortcut}=\"{IsAddShortcutProgram}\"");
        }

        /// <summary>
        /// �������� ��� ������ ���������.
        /// </summary>
        private void VisionOnSetupStarted()
        {
            //this.LogFileCreated
        }

        #endregion
    }
}
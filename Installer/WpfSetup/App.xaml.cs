﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Windows;
using CommonLib;
using WpfSetup.Common;
using WpfSetup.Localization;
using WpfSetup.Mvvm.ViewModel;

namespace WpfSetup
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        // Окно приветсявия.
        private static Window _splashscreen;

        public static VisionSetup Setup { get; private set; }

        public static MainWindowViewModel MainWindowViewModel { get; set; }

        /// <summary>
        /// Обработка запуска приложения.
        /// </summary>
        private void ApplicationOnStartup(object sender, StartupEventArgs e)
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;
            AppDomain.CurrentDomain.ProcessExit += CurrentDomainOnProcessExit;

            Start();
        }

        private void Start()
        {
            // Показ банера пока основное окно не запустилось.
            _splashscreen = new SplashScreen();
            _splashscreen.Show();

            // Создание временной папки.
            Directory.CreateDirectory(Settings.TempPath);
            // Язык по умолчанию. ru-RU
            TranslationSource.Instance.CurrentCulture = new CultureInfo("ru-RU");

            // Иницициализация главного класса установки, распаковка необходимых ресурсов.
            InitSetup();
            DriversHelper.UnPackDrivers();


            // Старт основного окна установки.
            MainWindowViewModel = new MainWindowViewModel();
            var mainWindow = new Mvvm.View.MainWindow { DataContext = MainWindowViewModel };
            mainWindow.Closing += MainWindowViewModel.WindowOnClosing;
            MainWindow = mainWindow;
            MainWindow.Show();
        }

        /// <summary>
        /// Инициализация установочника.
        /// </summary>
        private void InitSetup()
        {
            // Распаковка пакета msi
            var msiData = WpfSetup.Properties.Resources.Setup;
            var msiFile = Path.Combine(Settings.TempPath, "Vision.msi");
            File.Delete(msiFile);
            using (var sr = new FileStream(msiFile, FileMode.CreateNew))
            {
                sr.Write(msiData, 0, msiData.Length);
            }

            Setup = new VisionSetup(msiFile);
        }

        /// <summary>
        /// Обработчик закрытия приложения.
        /// </summary>
        private void CurrentDomainOnProcessExit(object sender, EventArgs eventArgs)
        {
            // Удаление темп папки при закрытии.
            var batPath = Settings.TempPath + @"\Performing.bat";
            File.AppendAllText(batPath, "");

            var performing = new StreamWriter(batPath);
            performing.WriteLine("@echo off");
            performing.WriteLine($@"RD /S /Q ""{Settings.TempPath}""");
            performing.Close();

            var startInfo = new ProcessStartInfo(batPath)
            {
                WorkingDirectory = Settings.TempPath,
                Arguments = @"/b"
            };
            Process.Start(startInfo);
        }

        /// <summary>
        /// Обработка событий недоступа библиотек.
        /// </summary>
        private static Assembly CurrentDomainOnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            // Get assembly name
            var assemblyName = new AssemblyName(args.Name).Name + ".dll";

            // Get resource name
            switch (assemblyName)
            {
                case "WixSharp.Msi.dll":
                    return Assembly.Load(WpfSetup.Properties.Resources.WixSharp_Msi);
                case "WpfSetup.resources.dll":
                    var culture = new AssemblyName(args.Name).CultureInfo;
                    if (culture.ToString() == "en-US")
                    {
                        return Assembly.Load(WpfSetup.Properties.Resources.WpfSetup_resources_en_US);
                    }

                    return null;
                case "CommonLib.dll":
                    return Assembly.Load(WpfSetup.Properties.Resources.CommonLib);
            }

            return null;
        }

        /// <summary>
        /// Скрыть показ временного окна.
        /// </summary>
        public static void HideSplashScreen()
        {
            _splashscreen.Close();
        }
    }
}
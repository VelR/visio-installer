﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Xml;
using CommonLib;
using Microsoft.Win32;

namespace WpfSetup.Common
{
    public static class DriversHelper
    {
        private static bool? _isInstallFraemwork;
        private static bool? _isInstallFlyCapture;
        private static bool? _isInstallDirectx;

        /// <summary>
        /// Установлен ли фраемворк.
        /// </summary>
        public static bool IsInstallFraemwork
        {
            get
            {
                if (_isInstallFraemwork == null)
                {
                    var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\1049");
                    var isInstall = false;

                    if (reg != null)
                    {
                        var release = reg.GetValue("Release").ToString();
                        isInstall = release.StartsWith("4618");
                    }

                    _isInstallFraemwork = isInstall;
                }

                return (bool)_isInstallFraemwork;
            }
            set => _isInstallFraemwork = value;
        }

        /// <summary>
        /// Установлен ли FlyCapture.
        /// </summary>
        public static bool IsInstallFlyCapture
        {
            get
            {
                if (_isInstallFlyCapture == null)
                {
                    var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{9D944EA8-BAE3-403A-AB7F-1B597688FD69}");
                    _isInstallFlyCapture = (bool)(reg != null);
                }
                return (bool)_isInstallFlyCapture;
            }
            set => _isInstallFlyCapture = value;
        }

        /// <summary>
        /// Установлен ли Directx 10.
        /// </summary>
        public static bool IsInstallDirectx
        {
            get
            {
                if (_isInstallDirectx == null)
                {
                    Process.Start("dxdiag", "/x dxv.xml");
                    while (!File.Exists("dxv.xml"))
                        Thread.Sleep(1000);
                    var doc = new XmlDocument();
                    doc.Load("dxv.xml");
                    var dxd = doc.SelectSingleNode("//DxDiag");
                    var dxv = dxd.SelectSingleNode("//DirectXVersion");

                    var version = Convert.ToInt32(dxv.InnerText.Split(' ')[1]);
                    _isInstallDirectx = version > 10;
                }

                return (bool)_isInstallDirectx;
            }

            set => _isInstallDirectx = value;
        }

        /// <summary>
        /// Установка фраемворка.
        /// </summary>
        public static void InstallFraemwork()
        {
            var install = new Process
            {
                StartInfo =
                {
                    FileName = App.Setup.FraemworkExePath,
                    Arguments = @"/q /norestart /ChainingPackage ADMINDEPLOYMENT"
                    // /q - тихая установка
                    // /norestart - не перезагружать пк
                }
            };
            install.Start();
            install.WaitForExit();
        }

        /// <summary>
        /// Установка FlyCapture.
        /// </summary>
        public static void InstallFlyCapture()
        {
            var install = new Process
            {
                StartInfo =
                {
                    FileName = App.Setup.FlyCaptureExePath,
                    Arguments = @"/q"
                }
            };
            install.Start();
            install.WaitForExit();
        }
        
        /// <summary>
        /// Установка Directx.
        /// </summary>
        public static void InstallDirectx()
        {
            var directxUnzipPath = $@"{Settings.TempPath}\directx\";

            Directory.CreateDirectory(directxUnzipPath);

            var directxUnPackage = new Process
            {
                StartInfo =
                {
                    FileName = App.Setup.DirectxExePath,
                    Arguments = $@"/q /t:{directxUnzipPath}"
                }
            };
            directxUnPackage.Start();
            directxUnPackage.WaitForExit();

            var instalDirectx = new Process
            {
                StartInfo =
                {
                    FileName = directxUnzipPath + "DXSETUP.exe",
                    Arguments = @"/silent"
                }
            };
            instalDirectx.Start();
            instalDirectx.WaitForExit();

            Directory.Delete(directxUnzipPath);
        }

        /// <summary>
        /// Распаковка файлов драйверов, для будущей установки.
        /// </summary>
        public static void UnPackDrivers()
        {
            // Распаковка пакета .net NDP472_KB4054530_x86_x64_AllOS_ENU
            if (!IsInstallFraemwork)
            {
                var netFraemworkFile = Path.Combine(Settings.TempPath, "NDP472_KB4054530_x86_x64_AllOS_ENU.exe");
                if (!File.Exists(netFraemworkFile))
                {
                    using (var sr = new FileStream(netFraemworkFile, FileMode.CreateNew))
                    {
                        sr.Write(Properties.Resources.NDP472_KB4054530_x86_x64_AllOS_ENU, 0,
                            Properties.Resources.NDP472_KB4054530_x86_x64_AllOS_ENU.Length);
                    }
                }

                App.Setup.FraemworkExePath = netFraemworkFile;
            }



            // Распаковка пакета FlyCapture_2_10_3_266_x64
            if (!IsInstallFlyCapture)
            {
                var flyCaptureFile = Path.Combine(Settings.TempPath, "FlyCapture_2_10_3_266_x64.exe");
                if (!File.Exists(flyCaptureFile))
                {
                    using (var sr = new FileStream(flyCaptureFile, FileMode.CreateNew))
                    {
                        sr.Write(Properties.Resources.FlyCapture_2_10_3_266_x64, 0,
                            Properties.Resources.FlyCapture_2_10_3_266_x64.Length);
                    }

                    App.Setup.FlyCaptureExePath = flyCaptureFile;
                }
            }
            
            //if (IsInstallDirectx)
            if(IsInstallDirectx)
            {
                var directxFile = Path.Combine(Settings.TempPath, "directx_Jun2010_redist.exe");
                if (!File.Exists(directxFile))
                {
                    using (var sr = new FileStream(directxFile, FileMode.CreateNew))
                    {
                        sr.Write(Properties.Resources.directx_Jun2010_redist, 0,
                            Properties.Resources.directx_Jun2010_redist.Length);
                    }

                    App.Setup.DirectxExePath = directxFile;
                }
            }
        }
    }
}

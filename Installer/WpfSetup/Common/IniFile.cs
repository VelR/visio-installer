﻿using System.Runtime.InteropServices;
using System.Text;

namespace WpfSetup.Common
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal,
            int size, string filePath);

        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        public IniFile(string iniPath)
        {
            path = iniPath;
        }
        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        public void IniWriteValue(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, path);
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        public string IniReadValue(string section, string key)
        {
            var temp = new StringBuilder(255);
            var i = GetPrivateProfileString(section, key, "", temp, 255, path);
            return temp.ToString();

        }
    }
}

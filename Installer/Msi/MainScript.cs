using System;
using CommonLib;
using WixSharp;

namespace Msi
{
    /// <summary>
    /// WixSharp S�ript, ������� MSI ����������
    /// </summary>
    public class MainScript
    {
        /// <summary>
        /// ���� ��� ����� ����� ������� ����� ����������.
        /// </summary>
        private const string ApplicationPath = @"G:\work\visio-installer\Application";
        private static readonly string VisionFilesPath = $@"{ApplicationPath}\Vision";
        private static readonly string ResourcesPath = $@"{ApplicationPath}\Resources";
        private static readonly string IconPath = $@"{ResourcesPath}\Icon.ico";

        static void Main()
        {
            BuildMainMsi();
        }

        private static void BuildMainMsi()
        {
            // � ������� ����� ��� ����� ��������� ������� �� ������ VisionSetup � ������ ��������� ��� ��������������.
            var applicationFeature = new Feature(Features.Application);

            //��������������� ������.
            var service = new File(applicationFeature, $@"{VisionFilesPath}\TestGeneratorServiceHOST.exe")
            {
                ServiceInstaller = new ServiceInstaller
                {
                    Name = "TestGeneratorServiceHOST.Svc",
                    StartOn = SvcEvent.Install,
                    StopOn = SvcEvent.InstallUninstall_Wait,
                    RemoveOn = SvcEvent.Uninstall_Wait,
                    DelayedAutoStart = true,
                    //ServiceSid = ServiceSid.none,
                    //FirstFailureActionType = FailureActionType.restart,
                    //SecondFailureActionType = FailureActionType.restart,
                    //ThirdFailureActionType = FailureActionType.runCommand,
                    //ProgramCommandLine = "MyApp.exe -run",
                    //RestartServiceDelayInSeconds = 30,
                    //ResetPeriodInDays = 1,
                    //PreShutdownDelay = 1000 * 60 * 3, 
                    //RebootMessage = "Failure actions do not specify reboot",
                }
            };

            var project =
                new Project(Settings.ApplicationName,
                    new Dir($"{Settings.ApplicationPath}",
                        // �������� ������.
                        service,
                        // ���������� �������� ��� ����� � ������� �� VisionFilesPath � ��������� �� � [INSTALLDIR]
                        new Files(applicationFeature, "*.*", s =>
                        {
                            if (service.Name == s) return false;
                            return true;
                        })),

                    // ��������� �������
                    new Dir("%Desktop%",
                        new ExeFileShortcut(applicationFeature, Settings.ApplicationName, $"[{Props.InstallDir}]HandmadeCollectorData.exe", arguments: "")
                        {
                            IconFile = IconPath,
                            Condition = new Condition($"{Props.InstallDesktopShortcut}=\"True\"")
                        }),

                    new Dir("%ProgramMenuFolder%",
                        new ExeFileShortcut(applicationFeature, Settings.ApplicationName, $"[{Props.InstallDir}]HandmadeCollectorData.exe", arguments: "")
                        {
                            IconFile = IconPath,
                            Condition = new Condition($"{Props.InstallStartprogramShortcut}=\"True\"")
                        }),

                    // HKEY_Local_Machine\SOFTWARE\AudioIstok\Vision
                    new RegValue(applicationFeature, RegistryHive.LocalMachine, $@"Software\{Settings.Manufacturer}\{Settings.ApplicationName}", "Version", Settings.Version),

                    // �������� � ����������
                    new Property(Props.InstallDesktopShortcut, "False"),
                    new Property(Props.InstallStartprogramShortcut, "False"));


            project.Platform = Platform.x64;
            project.SourceBaseDir = VisionFilesPath;
            project.UI = WUI.WixUI_Common;
            project.GUID = new Guid("6f330b47-2577-43ad-9095-1861ba25889b");
            project.PreserveTempFiles = true;
            project.InstallScope = InstallScope.perMachine;

            // ���������� � �������� ��� ����� � "��������� � �������� ��������".
            project.ControlPanelInfo.ProductIcon = IconPath;
            project.ControlPanelInfo.Manufacturer = Settings.Manufacturer;
            project.Version = new Version(Settings.Version);

            // ��� ������� ���������� ������ ������ ����� ������� ����������, ������������ ������� �� �������� � �������� ������.
            project.MajorUpgrade = new MajorUpgrade
            {
                Schedule = UpgradeSchedule.afterInstallInitialize,
                DowngradeErrorMessage = "A later version of [ProductName] is already installed. Setup will now exit."
            };

            project.BuildMsi();
        }
    }
}

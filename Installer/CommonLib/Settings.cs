﻿using System;
using System.IO;

namespace CommonLib
{
    public static class Settings
    {
        /// <summary>
        /// Производитель.
        /// </summary>
        public const string Manufacturer = "IstokAudio";

        /// <summary>
        /// Имя App.
        /// </summary>
        public const string ApplicationName = "Vision";

        #region Пути

        /// <summary>
        /// Путь установки приложения.
        /// </summary>
        public static string ApplicationPath => Path.Combine(ProgramFilesPath, Manufacturer, ApplicationName);

        /// <summary>
        /// Путь установки БД для видео.
        /// </summary>
        public static string BigDbPath => Path.Combine(ProgramDataAppFolder, "VideoDb");

        /// <summary>
        /// Путь установки малой БД.
        /// </summary>
        public static string SmallDbPath => Path.Combine(ProgramDataAppFolder, "Db");


        /// <summary>
        /// Путь до Папки IstokAudio в ProgramData.
        /// </summary>
        public static string ProgramDataManufacturerFolder => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Manufacturer);

        /// <summary>
        /// Путь до Папки программы в ProgramData.
        /// </summary>
        public static string ProgramDataAppFolder => Path.Combine(ProgramDataManufacturerFolder, ApplicationName);

        /// <summary>
        /// Путь до файла config.ini.
        /// </summary>
        public static string IniFilePath => Path.Combine(ProgramDataAppFolder, "config.ini");

        /// <summary>
        /// Путь до папки Program Files в зависимости от разрядности системы.
        /// </summary>
        internal static string ProgramFilesPath => Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);

        /// <summary>
        /// Путь до темповой папки.
        /// </summary>
        public static string TempPath => Path.GetTempPath() + $@"{ApplicationName}\";

        /// <summary>
        /// Имя основного exe.
        /// </summary>
        public static string MainExeFileName => "HandmadeCollectorData.exe";

        #endregion

        public static string Version => "1.0.0.0";

        // Необходимый размер в гигах.
        public static string RequiredSize = "3";
        public static string RequiredBigDbSize = "500";
        public static string TotalRam = "16";

    }
}

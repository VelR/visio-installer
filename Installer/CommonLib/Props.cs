﻿namespace CommonLib
{
    /// <summary>
    /// Класс предоставляет общий доступ к именам свойств.
    /// </summary>
    public static class Props
    {
        /// <summary>
        /// Папка установки основного приложения.
        /// </summary>
        public const string InstallDir = "INSTALLDIR";

        /// <summary>
        /// Установливать ли ярлык на рабочий стол.
        /// </summary>
        public const string InstallDesktopShortcut = "INSTALLDESKTOPSHORTCUT";


        /// <summary>
        /// Установливать ли ярлык в ProgramMenuFolder.
        /// </summary>
        public const string InstallStartprogramShortcut = "INSTALLSTARTPROGRAMSHORTCUT";
    }
}

﻿namespace CommonLib
{
    /// <summary>
    /// Класс предоставляет общий доступ к фичам сборки.
    /// </summary>
    public static class Features
    {
        /// <summary>
        /// Установка основного приложения.
        /// </summary>
        public const string Application = "Application";
    }
}

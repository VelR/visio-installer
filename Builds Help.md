**Алгоритмы изменения и сборки установочника:**

**Изменение в проекте CommonLib**

	1. После изменений для правильной работы WpfSetup необходимо:
		- Скопировать или заменить CommonLib.dll в WpfSetup\Resources


**Изменение Wix скрипта (проект Msi)**

	1. Если была изменена библиотека "CommonLib" 
		- Сохраненить изменения в библиотеке;
		- Собрать библиотеку. (Build)
	2. Сборка MSI:	
		- Делаем обычный Build проекта Msi;
		- В папке bin\Debug запустить Msi.exe
			Таким образом запускается скрипт сборки Vision.msi
			После завершению работы файла Msi.exe
				(Если нет ошибок в скрипте)
					Vision.msi - создан.
			Примечание!
				в этой же папке после запуска Msi.exe будет сгенерирован файл Vision.wxs
				это и есть wix скрипт на основании которого будет собран Vision.msi
				Можно залезть внутрь файла и убедиться какая версия у установочника, 
				и какие файлы добвалены в Vision.exe.

		Важно! 
			1. Vision.msi является установочником который устанавливает Vision без управления, 
				без дополнительных драйверов.
			2. Vision.msi используется в проекте WpfSetup.exe и его нужно заменить в WpfSetup/Resources
	3. После изменений для правильной работы WpfSetup необходимо:
		- Скопировать или заменить Vision.msi в WpfSetup\Resources

Изменение в проекте WpfSetup
	
		1. Если Были изменения в проекте CommonLib, то
			- Скопировать или заменить CommonLib\CommonLib.dll в WpfSetup\Resources

		2. Если Были изменения в проекте Msi, то
			- Скопировать или заменить Msi\bin\debug(relise)\Vision.msi в WpfSetup\Resources
				смотри пункт "Изменение Wix скрипта (проект Msi)";

		3. Если были изменения в WpfSetup\Localization\Resources\Strings.en-US.resx
			- Сделать билд WpfSetup затем:
			- Скопировать c переименовыванием
				WpfSetup\bin\Debug\en-US\WpfSetup.resources.dll в 
				WpfSetup\Resources\WpfSetup.resources.en-US.dll					

		4. После пунтктов 1,2 или 3 
			- Clean + Build или Rebuild; 
**Необходимо установить на рабочую машину (используется в проекте Msi):**
        
	https://github.com/wixtoolset/wix3/releases/download/wix3111rtm/wix311.exe
	
Решение состоит из трех проекто

**Msi, проект который собирает установочник msi, для основного приложения.**

Проект Msi, использует технологию WixSharp https://github.com/oleg-shilo/wixsharp/wiki/Deployment-scenarios#complete-custom-external-ui

WixSharp с помощью своего синтаксиса генерирует промежуточный WixFile и на его основе собирает Msi

MainScript.cs - Файл где описан скрипт сборки MSI

    !ВАЖНО! 
    ApplicationPath - необходимо указать директорию где хранятся папки Vision(релизная) и Resources (иконка, iniFile)
    
**Проект WpfSetup**

Ниже перечислены самые необходимые классы

    VisionSetup.cs          - Класс отвечает зв установку. (StartInstall, StartUninstall...)
    
    AfterInstallAction.cs   - Действия после завершения установки msi пакета.
    
    Common\Setting.cs       - Хранит информацию о продукте и используемые пути.
    Сommon\DriversHelper.cs - Отвечает за установку драйверов.
    Common\IniFile.cs       - За работу с config.ini.
    
    Mvvm\ViewModel\MainWindowViewModel.cs - Контроллер шагов установки, тут вся логика, какой шаг за каким идет, + контроль за управление кнопок.
    Mvvm\ViewModel\Steps\BaseStepViewModel.cs - Базовые возможности всех шагов, все *StepViewModel наследуют этот класс.
    
    Mvvm\View\MainWindowView.xaml - 
        Содержит ContentControl предназначен для отображения степа, в зависимости от выбранного ViewModel шага
        Содержит панель кнопок шагов, их инициализация производиться в каждом *StepViewModel отдельно на основе BaseStepViewModel
        
    WpfSetup\Mvvm\Common\AnaliseHelper.cs - Предназначен для анализа системных требований
        